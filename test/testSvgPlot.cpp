//
// Created by expert on 27-03-18.
//

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <SvgPlotter.h>
#include <vector>

TEST_CASE( "A plot will make the resulting vector bigger")
{
	SvgPlot::SvgPlotter plotter;
	std::vector< char > v;
	auto inputter = std::back_inserter( v );
	auto output = plotter.plot( inputter );
	REQUIRE( v.size() > 0 );
}

TEST_CASE( "Char arrays can be used as outputs")
{
	SvgPlot::SvgPlotter plotter;
	char output[ 1024 ];
	auto end = plotter.plot( output );
	REQUIRE( std::distance( output, end ) );
	*end = '\0';
	std::cout << output;
}
