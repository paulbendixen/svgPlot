#ifndef SVGPLOT_LIBRARY_H
#define SVGPLOT_LIBRARY_H

#include <vector>
#include <ostream>

namespace SvgPlot
{

struct Point
{
	double x;
	double y;
};

class SvgPlotter
{
	public:
		SvgPlotter() = default;
		explicit SvgPlotter( std::vector< Point > pointVector );

		template< typename outputIterator >
		outputIterator plot( outputIterator out );

	private:
		friend std::ostream& operator<<( std::ostream& output, const SvgPlotter& plotter );
		std::vector< Point > data;

		static const constexpr char header[] =
		R"escape(<svg width="{0}" height="{1}" viewBox="{2} {3} {0} {1}" xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"> )escape";;
		static const constexpr char defs[] = R"esc(	<defs>
		<pattern id="smallgrid" width="10" height="10" patternUnits="userSpaceOnUse">
			<path d="M 0 10 L 0 0 10 0" fill="none" stroke-width="0.5" stroke="lightgray"/>
		</pattern>
		<pattern id="grid" width="100" height="100" patternUnits="userSpaceOnUse">
			<rect x="0" y="0" width="100" height="100" fill="url(#smallgrid)"/>
			<path d="M 0 100 L 0 0 100 0" fill="none" stroke-width="1.0" stroke="black"/>
		</pattern>
		<marker id="arrow" markeWidth="5" markerHeight="5" orient="auto" refX="0" refY="2.5">
			<path d="M 0 0 L 2.5 2.5 0 5 Z" />
		</marker>
	</defs>
	<rect x="-250" y="-250" width="501" height="501" fill="url(#grid)"/>
	<path d="M 0 250 L 0 -245" fill="none" stroke="black" stroke-width="2" marker-end="url(#arrow)"/>
	<path d="M -250 0 L 245 0" fill="none" stroke="black" stroke-width="2" marker-end="url(#arrow)"/>
)esc";
		static const constexpr char footer[] = R"(</svg>)";

		template< typename outputIterator, typename pointIterator >
		outputIterator addPlot( outputIterator destination, pointIterator dataIterator )
		{
			return destination;
		};
};

template< typename outputIterator >
outputIterator SvgPlotter::plot( outputIterator out )
{
	auto endpoint = std::copy( std::begin( header ), std::end( header ) - 1, out );
	endpoint = std::copy( std::begin( defs ), std::end( defs ) - 1, endpoint );
	endpoint = addPlot( endpoint, std::begin( data ) );
	endpoint = std::copy( std::begin( footer ), std::end( footer ) - 1, endpoint );
	return endpoint;
}

}
#endif