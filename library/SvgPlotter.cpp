#include "SvgPlotter.h"

/*
 * 0: Total width of image
 * 1: Total height of image
 * 2: xMin - padding
 * 3: yMin - padding ( in flipped coordinates )
 * 4: xMin
 * 5: yMin
 * 6: xRange
 * 7: yRange
 * 8: datapoints string
 */

SvgPlot::SvgPlotter::SvgPlotter( std::vector< SvgPlot::Point > pointVector )
	:data( pointVector)
{
}

const constexpr char SvgPlot::SvgPlotter::header[];
const constexpr char SvgPlot::SvgPlotter::defs[];
const constexpr char SvgPlot::SvgPlotter::footer[];

